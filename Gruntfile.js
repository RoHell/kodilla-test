module.exports = function(grunt) {

	// Project configuration.
	grunt.initConfig({
		sass: {
      		options: {
        		sourceMap: true
      		},
      		dist: {
        		files: {
          			'css/style.css': 'scss/style.scss'
        		}
      		}
    	},

		imagemin: {
	    	dynamic: {
	        	files: [{
	            	expand: true,
	            	cwd: 'images/',
	            	src: ['**/*.{png,jpg,gif}'],
	            	dest: 'images/build/'
	        	}]
	    	}
		},

		watch: {
    		scripts: {
        		files: ['**/*.scss'],
        		tasks: ['sass'],
        		options: {
            		spawn: false,
        		}
    		}
		},

		postcss: {
    		options: {
      			map: true, // inline sourcemaps

      			// or
      			map: {
          			inline: false, // save all sourcemaps as separate files...
          			annotation: 'dist/css/maps/' // ...to the specified directory
      			},

      			processors: [
       				require('pixrem')(), // add fallbacks for rem units
        			require('autoprefixer')({browsers: 'last 2 versions'}), // add vendor prefixes
        			require('cssnano')() // minify the result
      			]
    		},
    		dist: {
      			src: 'css/*.css'
    		}
  		},

  		browserSync: {
            server: {
                bsFiles: {
                    src : [
                        'css/*.css',
                        '*.html'
                    ]
                },
                options: {
                    watchTask: true,
                    server: {
                    	baseDir: './'
                    }
                }
            }
        }
	})

	// Load the plugin tasks
	grunt.loadNpmTasks('grunt-sass');	
	grunt.loadNpmTasks('grunt-contrib-imagemin');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-postcss');
	grunt.loadNpmTasks('grunt-browser-sync');

	// Default task(s).
	grunt.registerTask('default', ['browserSync', 'sass', 'imagemin', 'watch', 'postcss']);

};